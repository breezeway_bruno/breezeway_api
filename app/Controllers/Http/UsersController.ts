import User from '../../Models/User'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, validator } from '@ioc:Adonis/Core/Validator'

export default class UsersController {
  public async index () {
    const users = await User.all()
    return users
  }

  public async store ({ request }: HttpContextContract) {
    const bodySchema = schema.create({
      name: schema.string(),
      password: schema.string(),
    })

    const dataValidate = await request.validate({
      schema: bodySchema,
      cacheKey: request.url(),
    })

    try {
      await User.create(dataValidate)
    } catch (e) {
      throw Error(e)
    }
  }
}
