import test from 'japa'

test.group('Unit Tests', () => {
  test('assert sum 1', (assert) => {
    assert.equal(2 + 2, 4)
  })
  test('assert sum 2', (assert) => {
    assert.equal(2 + 3, 5)
  })
  test('assert sum 3', (assert) => {
    assert.equal(2 + 1, 4)
  })
})
