import test from 'japa'
import User from '../../app/Models/User'

test.group('Integration Tests', () => {
  test('Ensure User registration method works', async (assert) => {
    const user = new User()
    user.name = 'user@email.com'
    user.password = '123'
    await user.save()

    assert.notEqual(user.password, '123')
  })
})
